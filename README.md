# PracticaGenomas

Una práctica OOP para la asignatura PER usando conceptos basados en el genoma.

Entrega probable el día 21 de Marzo con exámen el día 25.

## Objetivo

El objetivo de la prática es diseñar un primer esqueleto de clases que usaremos para el tratamiento de genes, secuencias, etc. En futuras prácticas iremos mejorando y creando un sistema mucho más complejo.

El alumno deberá entregar los siguientes elementos en un proyecto de gitlab:
-  Las clases en Python descritas más abajo
-  Una bateria de pruebas unitarias que comprueben el correcto funcionamiento de su código
-  Ficheros adicionales que considere necesarios. Por ejemplo, para activar las pruebas unitarias o para evitar subir a git ficheros auxiliares.
-   Un documento o Readme que describa el funcionamiento, principales característias, extras, etc. que considere oportuno destacar.

A continuación se describen estos elementos.

## Pasos previos a la práctica
Debe crear un proyecto privado en su cuenta de gitlab con el nombre de **"PER22-p1-Genoma"**. **Es importante que mantenga ese nombre** ya que se utilizará para la corrección de la práctica.
El proyecto debe **ser privado y no visible** a ningún compañero. Se pasarán comprobaciones para detectar la copia de código. Si se detecta que el proyecto contiene códido y no es privado, el alumno **suspenderá automáticamente** la práctica.

Dentro de ese proyecto debe haber al menos tres ficheros llamados:
 -  "sequece.py"
 -  "region.py"
 -  "unittests.py". 
 
El primero será el código de las clases SequenceBase y Sequence en python. El segundo contendrá el código de la clase Region. Y el tercero es el test utilizado por el alumno para comprobar el funcionamiento de su práctica. 
Se recomienda que se añadan los ficheros que configura Gitlab para la integración continua (CI/CD)
En el proyecto podría haber otros ficheros adicionales tales como un documento explicando el funcionamiento de la práctica (readme) o respondiendo a las preguntas de los profesores.

## Contexto
Un nucleótido es la pieza básica de los ácidos nucleicos. El ARN y el ADN son polímeros formados por largas cadenas de nucleótidos. Un nucleótido está formado por una molécula de azúcar (ribosa en el ARN o desoxirribosa en el ADN) unido a un grupo fosfato y una base nitrogenada. Las bases utilizadas en el ADN son la adenina (A), citosina (C), guanina (G) y timina (T). En el ARN, la base uracilo (U) ocupa el lugar de la timina.

Cada base tiene un complemento. El alumno puede obtener más información en la página de la wikipedia: https://es.wikipedia.org/wiki/Complementariedad_(biolog%C3%ADa_molecular)

Estos nucleótidos se unen formando cadenas que buscamos tratar usando Python.
Las cadenas o secuencias pueden tener información adicional como un nombre o una descripción.
Las cadenas pueden ocupar una region en un cromosoma. 

## Clases
El alumno deberá modelar tres clases llamadas "SequenceBase", "Sequence" y "Region". 

### SequenceBase
Una secuencia básica ("SequenceBase") utiliza una cadena de caracteres para representar una cadena de los nucleótidos que forman parte de un ácido nucleico (ADN y ARN). Cada carácter representa un nucleótido en la cadena. Por ejemplo, una posible secuencia de ADN sería "GTACCGAT".

La clase debe cumplir con los siguiente requisitos:
-   Un constructor que admita un string con la cadena de caracteres. Los caracteres que reciba en el constructor podrían estar en minúsculas o mayúsculas, pero SIEMPRE los guardará en mayúsculas. No admitirá una cadena de caracteres vacía.
-   Cuando se imprima una instancia de esta clase, se visualizará la cadena en mayúsculas.
-   Dos secuencias básicas serán iguales si tienen los mismos caracteres y en el mismo orden.
-   Deberá tener un método llamado "gc_content" que calcule el ratio de los nucleótidos GC en el total. Retornará un float con el ratio. El alumno puede probar esta funionalidad en la página web 
https://www.sciencebuddies.org/science-fair-projects/references/genomics-g-c-content-calculator
-   Deberá tener un método llamado "reverse_complement". Este método calculará una nueva secuencia invirtiendo (dando la vuelta a la cadena) y complementando los nucleótidos. El método retorna una nueva cadena y no modifica la cadena que ha recibido la llamada.  Por ejemplo, la cadena "ATGATGATG" se transformará en "CATCATCAT". El alumno puede probar otros casos en la página web:  https://www.bioinformatics.org/sms2/rev_comp.html
-   Deberá tener un método llamado "complement". Este método calculará una nueva secuencia complementando los nucleótidos. El método retorna una nueva cadena y no modifica la cadena que ha recibido la llamada. 

### Sequence
Una secuencia ("Sequence") es parecido a una SequenceBase salvo que tiene un nombre o identificador, un tipo (DNA, RNA, AA) y un posible comentario (opcional).

La clase debe cumplir con los siguiente requisitos:
-   Un constructor que admita un nombre. una cadena de caracteres con la secuencia y un tipo de secuencia (solo admitira DNA, RNA, AA). Opcionalmente podría tener un cuarto parámetro con una descripción o comentario. Se aplican los mismos requisitos descritos en SequenceBase. Si el tipo de secuencia no es uno de los tres permitidos, el constructor deberá levantar la excepción "ValueError".
-   Cuando se imprima una instancia de esta clase, se visualizará lusando el siguiente formato: "<SequenceID: {}, Seq: {}>", donde el primer hueco es el nombre de la secuencia y el segundo la cadena en mayúsculas.
-   Dos secuencias básicas serán iguales si tienen los mismos nombres, la misma cadena y el mismo tipo (el comentario, si existe, podría ser diferente).
-   Deberá tener los mismos métodos que una secuencia básica aunque teniendo en cuenta que debe devolver Secuence y no SecuenceBase. El nombre de la Secuence que devuelve se formará uniendo el nombre del método al nombre de la secuencia. Por ejemplo, si una secuencia se llama "chrM" el método reverse_complement retornará una secuencia llamada "reverse_complement_chrM"

### Region
Una región (Region) representa un segmento en un cromosoma (un segmento de un gen). Esta clase tendrá el nombre del cromosoma, la posición inicial y final de la secuencia y una secuencia de nucleótidos.

La clase debe cumplir con los siguiente requisitos:
-   Un constructor que admita un nombre de cromosoma, dos enteros con las posiciones iniciales y finales y una cadena de caracteres con la secuencia. Debe comprobar que las posiciones iniciales y finales son coherentes. En caso de que no sean coherentes, deberá levantar la excepción "ValueError".
-   Un métdo llamado "union" que realice la unión de dos regiones. Retorna una nueva región con el empalme de las dos regiones. La nueva región tendrá el nombre de la región más baja (la que tenga menor valor de inicio). El método deberá comprobar que dos regiones se pueden unir (no están solapadas y son contiguas). Si no se pueden unir, se levantará una excepción "ValueError".


## Pruebas básicas
Adicionalmente al código, el alumno deberá desarrollar su propia batería de tests unitarios. En la corrección de la práctica se evaluará el correcto desarrollo del código y la calidad de las pruebas. Es decir, la nota se pondrá en función de la calidad del código, así como el número y calidad de los tests realizados.

Se estima que será necesario un mínimo de unos 10-15 tests.

## Documentación

El alumno deberá incluir en el Readme un texto que explique las siguientes cuestiones:
-   ¿Ha usado herencia? ¿Dónde? Justifica las razones de haberla usado o no.
-   ¿Has usado sobreescritura? ¿Dónde? Justifica las razones de haberla usado o no.
-   ¿Has contemplado en tu diseño aspectos de visibilidad de atributos? Desarrolla este concepto e indica algún punto de tu código en el que se observe su uso (de haberlo usado).
-   ¿Alguna clase tiene referencias a otra clase? Es decir, ¿alguna clase tiene un atributo que sea una referencia a una instancia de otra clase?
-   Busca información sobre "polimorfismo" e intenta explicarlo con tus propias palabras. ¿Lo has usado en tu práctica?

## Para recuperar este proyecto
Recuerda que para obtener una copia del proyecto puedes realizar el siguiente comando en tu disco:

```
cd existing_repo
git clone https://gitlab.etsit.urjc.es/agustin.santos/practicagenomas.git
```

Sin embargo esto solo te hará una copia que internamente tiene referencias a mi proyecto. Si haces cambios no podrás subirlo a tu proyecto. Solo es útil para poder usar los ficheros que os incluyo. NO modifiques ese directorio si no quieres tener problemas.
